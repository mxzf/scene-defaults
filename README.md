# Scene Defaults
This module adds the ability to set defaults for scenes that are created in the future, using a configuration button in the game settings menu.

## Installation

Module manifest: https://gitlab.com/mxzf/scene-defaults/-/releases/permalink/latest/downloads/module.json
