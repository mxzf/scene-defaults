# Changelog

## 2.0.1 - V12 version

Added in V12 logic for handing the new fog data storage paths

## 2.0.0 - Package takeover version bump

Bumping the version to reflect taking over a package ID that already existed and was at 1.0.x already

## 0.1.0 - Initial relase

Initial release of the module

### Features

* Added a config menu button in the settings to configure default settings for future scenes.  

## 