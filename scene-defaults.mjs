class DefaultSceneConfig extends SceneConfig {
    constructor(object, options={}) {
        super(new Scene(game.settings.get('scene-defaults','config')), options);
    }
    _canUserView(user) {return user.isGM}
    
    async _updateObject(event, formData) {
        // Copy data nulling logic from core
        
        // SceneData.texture.src is nullable in the schema, causing an empty string to be initialised to null. We need to
        // match that logic here to ensure that comparisons to the existing scene image are accurate.
        if ( formData["background.src"] === "" ) formData["background.src"] = null;
            if ( formData.foreground === "" ) formData.foreground = null;
        if (foundry.utils.isNewerVersion(game.version, 12)) {
            if ( formData["fog.overlay"] === "" ) formData["fog.overlay"] = null;
            if ( formData["fog.colors.unexplored"] === "" ) formData["fog.colors.unexplored"] = null;
            if ( formData["fog.colors.explored"] === "" ) formData["fog.colors.explored"] = null;
        }
        else { // V11 stuff
            // Toggle global illumination threshold
            if ( formData.hasGlobalThreshold === false ) formData.globalLightThreshold = null;
            delete formData.hasGlobalThreshold;
            if ( formData.fogOverlay === "" ) formData.fogOverlay = null;

            // The same for fog colors
            if ( formData.fogUnexploredColor === "" ) formData.fogUnexploredColor = null;
            if ( formData.fogExploredColor === "" ) formData.fogExploredColor = null;
        }
        game.settings.set('scene-defaults', 'config', foundry.utils.expandObject(formData))
    }
}

Hooks.on('init', () => {
    game.settings.register('scene-defaults', 'config', {
        scope: 'world',
        config: false,
        type: Object,
        default: {name: 'Prototype Scene'}
    })

    game.settings.registerMenu("scene-defaults", "defaults_config", {
      name: "SCENE-DEFAULTS.settings.config.title",
      label: "SCENE-DEFAULTS.settings.config.label",
      hint: "SCENE-DEFAULTS.settings.config.hint",
      icon: "fas fa-cogs",
      type: DefaultSceneConfig,
      restricted: false
    });
})

Hooks.on('preCreateScene', (doc, data, options, userId) => {
    doc.updateSource(foundry.utils.mergeObject(game.settings.get('scene-defaults','config'), data, {inplace:false}))
})